{
  description = "A game stub written in Haskell";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
    affection-src.url = "git+https://gitea.nek0.eu/nek0/affection";
  };

  outputs = { self, nixpkgs, flake-utils, affection-src }:
    flake-utils.lib.eachSystem [
      "aarch64-linux"
      "i686-linux"
      "x86_64-linux"
    ] (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        haskellPackages = pkgs.haskellPackages;

        jailbreakUnbreak = pkg:
          pkgs.haskell.lib.doJailbreak (pkg.overrideAttrs (_: { meta = { }; }));

        packageName = "tracer-game";
      in {
        packages.${packageName} = # (ref:haskell-package-def)
          (haskellPackages.callCabal2nix packageName self rec {
            affection = affection-src.defaultPackage.${system};
          }).overrideAttrs (oa:
            let
              inherit (nixpkgs.legacyPackages.${system})
                pkg-config makeWrapper
                copyDesktopItems makeDesktopItem;
            in {
              nativeBuildInputs = oa.nativeBuildInputs ++ [
                pkg-config makeWrapper
                nixpkgs.legacyPackages.${system}.copyDesktopItems
              ];
              desktopItems = [ (makeDesktopItem {
                name = "Tracer";
                desktopName = "nek0's tracer";
                categories = [ "Game" ];
                exec = "tracer";
              }) ];
              installPhase = ''
                ${oa.installPhase}

                mkdir -p $out/share/tracer
                cp -r assets $out/share/tracer/assets
                wrapProgram $out/bin/tracer-game \
                  --chdir $out/share/tracer
              '';
            });

        defaultPackage = self.packages.${system}.${packageName};

        devShell = pkgs.mkShell {
          buildInputs = with haskellPackages; [
            haskell-language-server
            ghcid
            cabal-install
          ];
          inputsFrom = builtins.attrValues self.packages.${system};
        };

        hydraJobs = builtins.mapAttrs (_:
          nixpkgs.lib.hydraJob
        ) self.packages.${system};
      });
}


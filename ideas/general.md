# General gameplay Idea

An infiltration game with less aspects of breaking and entering but social
engineering.

There can be some hacking/code breaking aspects to get information needed for
further advancement.

Player gathers Points/Money

fighting à la "Berzerk"? -- no

G.O.D. (Grid Overwatch Division) -- Greetings from Shadowrun ^^ -- no
R.I.P. (Reinhardt Industries Police) -- private police force -- yes

toilets. toilets everywhere.

## Setting

Reinhardt Industries Office Tower, Year 20??

## Story

The player is to enter the Reinhardt Industries Tower and steal their darabase
of account data.

## View

Isometric perspective

## Mechanics

password hints leading to social media content

bank account information -- get money -> bonus points?

NFC credit card information -- see bank account

gather keycards for doors and/or elevators

### NPC difficulty

lowest difficulty -- Someone who uses the same password everywhere

passwords as post-it on monitor

# tracer

**Diclaimer:**

Tracer is an ongoing project, which is still under heavy development.

## About the project

This is my first attempt at a game, which is not a clone or port of anything.

I've been working on it since February 2018 and is to date not finished. The
game itself is written entirely in [Haskell](https://haskell.org), using my own
game engine [Affection](https://github.com/nek0/affection).

I aim to get the game into a presentable, playable state in September 2018 for
the conference [Datenspuren](https://datenspuren.de/2018). But there is still
quite a lot to do until then...

## About the game

In this game you play an anonymous hacker/spy for hire. You have recently been
hired to steal the database of a company. The database is stored offline 
on-site at the company's headquarters, where you set out to intrude using means
of social engineering.

## Building

For building the game you need an installation of `ghc` and `cabal-install`.

### NixOS

There is a `shell.nix` file to build you a nix-shell environment as build
environment. There is only one assumption in this file:

To get the build environment, just run

```
nix-shell shell.nix
```

in a console from the project's folder.

After the environment has been built, run 

```
cabal run
```

to build and run the game's executable.


### Other GNU/Linux systems

#### System dependencies

To get the build environment, make sure you have a recent version (>= 8.4.3)
of `ghc` installed acompanied by a matching `cabal-install`. Both of these are
available from <https://haskell.org> in a bundle called `haskell-platform`. You
can also check your system's package manager for it.

Additionally you need the development libraries for:

* sdl2
* libglew

#### Haskell sandbox

Before you install any haskell packages I recommend highly to build a sandbox
for the project. To do this, invoke

```
cabal sandbox init
```

in a terminal in the project folder.

Now your sandbox is ready for installing all haskell dependencies by invoking

```
cabal install --only-dependencies -j
```

This may take some time, depending on your hardware.

#### Compiling the game

To compile the game, run

```
cabal build
```

in the sandbox.

#### Running the game

To run the game, invoke

```
cabal run
```

in the sandbox.

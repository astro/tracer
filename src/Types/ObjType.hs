{-# LANGUAGE MultiParamTypeClasses #-}
module Types.ObjType where

data ObjType
  = ObjDoor
  | ObjCopier
  | ObjComputer
  | ObjToilet
  deriving (Show, Eq, Ord, Enum)

type ObjState = String

-- class ObjectAction us otype ostate where
--   objectAction :: otype -> ostate -> Ent -> Affection us ()
-- 
--   objectTransition :: otype -> ostate -> Ent -> Affection us Entity
-- 
-- class ActionTime otype ostate where
--   actionTime :: otype -> ostate -> Double

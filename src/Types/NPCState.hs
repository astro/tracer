module Types.NPCState where

import Control.Concurrent.MVar (MVar)
import Linear (V2)

data NPCMoveState
  = NPCWalking
    { npcWalkPath :: [[V2 Int]]
    }
  | NPCStanding
    { npcStandTime  :: Double
    , npcFuturePath :: MVar [[V2 Int]]
    }

data NPCActionState
  = ASWork
  | ASToilet
  | ASDrink
  | ASEat
  | ASRandWalk
  deriving (Eq)

data NPCStats = NPCStats
  { statAttention :: Double
  , statBladder   :: Double
  , statThirst    :: Double
  , statHunger    :: Double
  , statFood      :: Double
  , statDrink     :: Double
  }

module Types.FontId where

data FontId
  = FontBedstead
  deriving (Show, Eq, Ord, Enum)

module Types.ReachPoint where

import Linear (V2(..))
import Types.Direction

data ReachPoint = ReachPoint
  { pointType      :: PointType
  , pointCoord     :: V2 Int
  , pointDir       :: Direction
  , pointClearance :: Word
  }
  deriving (Eq, Show)

data PointType
  = RoomExit
  | Table
  | Copier
  | Computer
  | Toilet
  | Drink
  | Eat
  | Elevator
  deriving (Eq, Show)

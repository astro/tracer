{-# LANGUAGE MultiParamTypeClasses #-}
module Types.Collidible where

-- internal imports

import Types.Map (Boundaries(..))
import Types.ImgId (ImgId(..))
import Types.Animation (AnimState(..))

class Collidible c where
  collisionObstacle :: c -> [(Boundaries Double)]

instance Collidible ImgId where
  collisionObstacle ImgMiscBox1 =
    [ Boundaries (0.2, 0.34) (0.8, 1)
    ]
  collisionObstacle ImgWallAsc =
    [ Boundaries (0.34, 0) (0.66, 1)
    ]
  collisionObstacle ImgWallDesc =
    [ Boundaries (0, 0.34) (1, 0.66)
    ]
  collisionObstacle ImgWallCornerN =
    [ Boundaries (0, 0.34) (0.66, 0.66)
    , Boundaries (0.34, 0.34) (0.66, 1)
    ]
  collisionObstacle ImgWallCornerE =
    [ Boundaries (0.34, 0.34) (1, 0.66)
    , Boundaries (0.34, 0.34) (0.66, 1)
    ]
  collisionObstacle ImgWallCornerS =
    [ Boundaries (0.34, 0.34) (1, 0.66)
    , Boundaries (0.34, 0) (0.66, 0.66)
    ]
  collisionObstacle ImgWallCornerW =
    [ Boundaries (0, 0.34) (0.66, 0.66)
    , Boundaries (0.34, 0) (0.66, 0.66)
    ]
  collisionObstacle ImgWallTNE =
    [ Boundaries (0, 0.34) (1, 0.66)
    , Boundaries (0.34, 0.34) (0.66, 1)
    ]
  collisionObstacle ImgWallTSW =
    [ Boundaries (0, 0.34) (1, 0.66)
    , Boundaries (0.34, 0) (0.66, 0.66)
    ]
  collisionObstacle ImgWallTSE =
    [ Boundaries (0.34, 0) (0.66, 1)
    , Boundaries (0.34, 0.34) (1, 0.66)
    ]
  collisionObstacle ImgWallTNW =
    [ Boundaries (0.34, 0) (0.66, 1)
    , Boundaries (0, 0.34) (0.66, 0.66)
    ]
  collisionObstacle ImgWallCross =
    [ Boundaries (0.34, 0) (0.66, 1)
    , Boundaries (0, 0.34) (1, 0.66)
    ]
  collisionObstacle ImgTableSW =
    [ Boundaries (0, 0.34) (1, 1)
    ]
  collisionObstacle ImgTableNW =
    [ Boundaries (0.34, 0) (1, 1)
    ]
  collisionObstacle ImgTableNE =
    [ Boundaries (0, 0) (1, 0.63)
    ]
  collisionObstacle ImgTableSE =
    [ Boundaries (0, 0) (0.63, 1)
    ]
  collisionObstacle ImgTableCorner =
    [ Boundaries (0, 0) (0.63, 1)
    , Boundaries (0, 0.34) (1, 1)
    ]
  collisionObstacle ImgTableC1 =
    [ Boundaries (0, 13/36) (13/36, 1)
    ]
  collisionObstacle ImgTableC2 =
    [ Boundaries (0, 0) (13/36, 13/36)
    ]
  collisionObstacle ImgTableC3 =
    [ Boundaries (1-13/36, 0) (1, 1-13/36)
    ]
  collisionObstacle ImgTableC4 =
    [ Boundaries (1-13/36, 1-13/36) (1, 1)
    ]
  collisionObstacle ImgMiscFlipchart =
    [ Boundaries (0, 7/36) (18/36, 28/36)
    ]
  collisionObstacle ImgMiscPlant1 =
    [ Boundaries (9/36, 9/36) (27/36, 27/36)
    ]
  collisionObstacle ImgMiscPlant2 =
    [ Boundaries (9/36, 9/36) (27/36, 27/36)
    ]
  collisionObstacle ImgMiscWatercooler =
    [ Boundaries (14/36, 14/36) (22/36, 22/36)
    ]
  collisionObstacle ImgMiscVending =
    [ Boundaries (0, 2/36) (24/36, 34/36)
    ]
  collisionObstacle ImgCabinetCoffeeSW = collisionObstacle ImgTableSW
  collisionObstacle ImgCabinetCoffeeSE = collisionObstacle ImgTableSE
  collisionObstacle ImgCabinetSinkSW = collisionObstacle ImgTableSW
  collisionObstacle ImgCabinetSinkSE = collisionObstacle ImgTableSE
  collisionObstacle ImgCabinetStoveSW = collisionObstacle ImgTableSW
  collisionObstacle ImgCabinetStoveSE = collisionObstacle ImgTableSE
  collisionObstacle ImgCabinetSW = collisionObstacle ImgTableSW
  collisionObstacle ImgCabinetSE = collisionObstacle ImgTableSE
  collisionObstacle ImgCabinetCorner = collisionObstacle ImgTableCorner
  collisionObstacle ImgEmptyNoWalk =
    [ Boundaries (0, 0) (1, 1) ]
  collisionObstacle _ = []

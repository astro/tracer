module Types.Animation where

import NanoVG (Image)

import Types.Direction

data AnimId = AnimId
  { aiVariation :: AnimationName
  , aiName      :: String
  , aiDirection :: Direction
  }
  deriving (Show, Eq, Ord)

data AnimState = AnimState
  { asId           :: AnimId
  , asCurrentFrame :: Int
  , asElapsedTime  :: Double
  }
  deriving (Show, Eq)

data AnimPlayback
  = APLoop
  | APOnce
  deriving (Show, Eq)

data Animation = Animation
  { animDuration :: Double
  , animSprites  :: [Image]
  , animPlay     :: AnimPlayback
  }
  deriving (Show, Eq)

data AnimationConfig = AnimationConfig
  { animConfOffset   :: (Int, Int)
  , animConfSize     :: (Int, Int)
  , animConfStep     :: (Int, Int)
  , animConfCount    :: Int
  , animConfDuration :: Double
  , animConfPlay     :: AnimPlayback
  }
  deriving (Show, Eq)

data AnimationName
  = AnimIntruder
  | AnimCopier
  | AnimComputer
  | AnimToilet
  | AnimJDoeM
  | AnimPoliceM
  | AnimDoor0
  deriving (Show, Eq, Ord, Enum)

isWallLike :: AnimationName -> Bool
isWallLike AnimDoor0 = True
isWallLike _ = False

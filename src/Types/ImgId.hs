module Types.ImgId where

data IconId
  = IconContrBlue
  | IconContrGreen
  | IconKbdBlue
  | IconKbdGreen
  | IconArrow
  deriving (Show, Eq, Ord, Enum)

data ImgId
  = ImgEmpty -- TODO: Find better solution than empty image.
  | ImgEmptyNoWalk
  | ImgWallAsc
  | ImgWallDesc
  | ImgWallCornerN
  | ImgWallCornerE
  | ImgWallCornerS
  | ImgWallCornerW
  | ImgWallTNE
  | ImgWallTSE
  | ImgWallTSW
  | ImgWallTNW
  | ImgWallCross
  | ImgMiscBox1
  | ImgTableSW
  | ImgTableNW
  | ImgTableNE
  | ImgTableSE
  | ImgTableCorner
  | ImgTableC1
  | ImgTableC2
  | ImgTableC3
  | ImgTableC4
  | ImgMiscFlipchart
  | ImgMiscPlant1
  | ImgMiscPlant2
  | ImgMiscWatercooler
  | ImgMiscVending
  | ImgCabinetCoffeeSW
  | ImgCabinetCoffeeSE
  | ImgCabinetSinkSW
  | ImgCabinetSinkSE
  | ImgCabinetStoveSW
  | ImgCabinetStoveSE
  | ImgCabinetSW
  | ImgCabinetSE
  | ImgCabinetCorner
  -- | ImgIntrNE
  -- | ImgIntrE
  -- | ImgIntrSE
  -- | ImgIntrS
  -- | ImgIntrSW
  -- | ImgIntrW
  -- | ImgIntrNW
  -- | ImgIntrN
  deriving (Show, Eq, Ord, Enum)

isWall :: ImgId -> Bool
isWall ImgWallAsc = True
isWall ImgWallDesc = True
isWall ImgWallCornerN = True
isWall ImgWallCornerE = True
isWall ImgWallCornerS = True
isWall ImgWallCornerW = True
isWall ImgWallTNE = True
isWall ImgWallTSE = True
isWall ImgWallTSW = True
isWall ImgWallTNW = True
isWall ImgWallCross = True
isWall _ = False

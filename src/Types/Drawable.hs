{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Types.Drawable where

import qualified Data.Map.Strict as Map

import Control.Concurrent.MVar

import NanoVG

import Foreign.C.Types (CFloat(..))

-- internal imports

import Types.Animation
import Types.UserData

class NanoDrawable us a where
  draw :: us -> CFloat -> CFloat -> CFloat -> CFloat -> CFloat -> a -> IO ()

instance NanoDrawable UserData Image where
  draw us x y w h alpha img = do
    let ctx = nano us
    save ctx
    beginPath ctx
    paint <- imagePattern ctx x y w h 0 img alpha
    rect ctx x y w h
    fillPaint ctx paint
    closePath ctx
    fill ctx
    restore ctx

instance NanoDrawable UserData AnimState where
  draw us x y w h alpha as = do
    anims <- readMVar (assetAnimations us)
    let a = anims Map.! asId as
        img = (animSprites a !! asCurrentFrame as) :: Image
    draw us x y w h alpha img

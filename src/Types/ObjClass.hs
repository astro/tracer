{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
module Types.ObjClass where

import Affection

import Control.Monad.Trans.Resource (ResIO)
import Data.Ecstasy

import Types.Entity

class ObjectAction otype ostate where
  objectAction
    :: Double
    -> otype
    -> ostate
    -> Ent
    -> SystemT Entity (AffectionState AffectionData ResIO) ()

  objectTransition
    :: otype
    -> ostate
    -> Bool
    -> Ent
    -> Maybe Ent
    -> SystemT Entity (AffectionState AffectionData ResIO) (Entity 'SetterOf)

class ActionTime otype ostate where
  actionTime :: otype -> ostate -> Double

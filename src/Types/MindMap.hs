module Types.MindMap where

import Linear (V2(..))

data MMNode = MMNode
  { mmPos :: V2 Double
  , mmId  :: Int
  }
  deriving (Show)

instance Eq MMNode where
  n1 == n2 = mmId n1 == mmId n2

instance Ord MMNode where
  compare n1 n2 = compare (mmId n1) (mmId n2)

module Types.StateData where

import Data.Matrix
import Data.Map
import Data.Vector as V

import NanoVG hiding (V2)

import Linear (V2)

import Types.ReachPoint
import Types.Map
import Types.ImgId
import Types.Animation
import Types.Direction

data StateData
  = None
  | LoadData
    { loadAssetImages :: Map ImgId Image
    , loadAssetAnims  :: Map AnimId Animation
    , loadAssetIcons  :: Map IconId Image
    }
  | MainData
    { mapMat      :: Matrix TileState
    , imgMat      :: Matrix (Maybe ImgId)
    , reachPoints :: V.Vector ReachPoint
    , mmImgMat    :: Matrix (Maybe ImgId)
    , roomGraph   :: V.Vector Graph
    }
  | MenuData
    { velocity  :: V2 Double
    , rotation  :: Direction
    , activate  :: Double
    , switchMap :: Double
    }
  deriving (Eq)

module Types.Direction where

data Direction
  = NE
  | E
  | SE
  | S
  | SW
  | W
  | NW
  | N
  deriving (Show, Eq, Ord, Enum)

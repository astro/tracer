module Types.Entity where

import Data.Ecstasy

import Linear (V2)

import Types.Direction
import Types.Map
import Types.NPCState
import Types.Animation
import Types.ObjType
import Types.ReachPoint

data Entity f = Entity
  { pos                :: Component f 'Field (V2 Double)
  , mmpos              :: Component f 'Field (V2 Double)
  , gridPos            :: Component f 'Field (V2 Int)
  , vel                :: Component f 'Field (V2 Double)
  , xyvel              :: Component f 'Field (V2 Double)
  , mmvel              :: Component f 'Field (V2 Double)
  , velFact            :: Component f 'Field Double
  , rot                :: Component f 'Field Direction
  , obstacle           :: Component f 'Field (Boundaries Double)
  , ignoreObstacle     :: Component f 'Field ()
  , player             :: Component f 'Unique ()
  , npcMoveState       :: Component f 'Field NPCMoveState
  , npcWorkplace       :: Component f 'Field ReachPoint
  , npcActionState     :: Component f 'Field NPCActionState
  , npcStats           :: Component f 'Field NPCStats
  , clearanceLvl       :: Component f 'Field Word
  , anim               :: Component f 'Field AnimState
  , objAccess          :: Component f 'Field [((V2 Int), Direction)]
  , objType            :: Component f 'Field ObjType
  , objState           :: Component f 'Field ObjState
  , objStateTime       :: Component f 'Field Double
  , objUsedBy          :: Component f 'Field Ent
  , objPlayerActivated :: Component f 'Field Bool
  , objSolved          :: Component f 'Field Bool
  }
  deriving (Generic)

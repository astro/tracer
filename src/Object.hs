{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
module Object where

import Data.Ecstasy

-- internal imports

import Types

import Object.Computer
import Object.Copier
import Object.Door

instance ObjectAction ObjType ObjState where
  objectAction dt t@ObjCopier s ent = copierObjectAction dt t s ent

  objectAction dt t@ObjComputer s ent = computerObjectAction dt t s ent

  objectAction dt t@ObjDoor s ent = doorObjectAction dt t s ent

  objectAction _ _ _ _ = return ()

  objectTransition t@ObjCopier s pa ent aent =
    copierObjectTransition t s pa ent aent

  objectTransition t@ObjComputer s pa ent aent =
    computerObjectTransition t s pa ent aent

  objectTransition t@ObjDoor s pa ent aent =
    doorObjectTransition t s pa ent aent

  objectTransition _ _ _ _ _ = return unchanged

module Animation where

import Types.Animation

animFloats :: AnimId -> Bool
animFloats (AnimId AnimComputer _ _) = True
animFloats _ = False

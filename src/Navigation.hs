module Navigation where

import Affection as A

import Data.Matrix as M
import Data.Maybe (isJust)

-- internal imports

import Types
import Util

isReachable
  :: Matrix (Maybe ImgId)
  -> [(V2 Double, Boundaries Double)]
  -> [V2 Int]
  -> [ReachPoint]
  -> Bool
isReachable imgmat animBounds reaches exits =
  let result =
        concatMap
          ((\exit -> map (astarAppl imgmat animBounds exit) reaches) . pointCoord)
          exits
  in  all isJust result

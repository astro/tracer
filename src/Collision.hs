module Collision where

import Linear (V2(..))

import Types

checkBoundsCollision2
  :: V2 Double
  -> V2 Double
  -> Double
  -> V2 Double
  -> Boundaries Double
  -> V2 Double
checkBoundsCollision2
  pre@(V2 pr pc) nex dt acc (Boundaries (minr, minc) (maxr, maxc))
    | colltr < dt && colltc < dt = V2 0 0
    | colltr < dt                = V2 0 0
    | colltc < dt                = V2 0 0
    | otherwise                  = acc
    where
      V2 vr vc = fmap (/ dt) (nex - pre)
      colltr
        | vr > 0 && prr <= maxr && (prc + 0.15 >= minc && prc - 0.15 <= maxc) =
          ((fromIntegral (floor pr :: Int) + minr - 0.15) - pr) / vr
        | vr < 0 && prr >= minr && (prc + 0.15 >= minc && prc - 0.15 <= maxc) =
          ((fromIntegral (floor pr :: Int) + maxr + 0.15) - pr) / vr
        | otherwise = dt
      colltc
        | vc > 0 && prc <= maxc && (prr + 0.15 >= minr && prr - 0.15 <= maxr) =
          ((fromIntegral (floor pc :: Int) + minc - 0.15) - pc) / vc
        | vc < 0 && prc >= minc && (prr + 0.15 >= minr && prr - 0.15 <= maxr) =
          ((fromIntegral (floor pc :: Int) + maxc + 0.15) - pc) / vc
        | otherwise = dt
      prr = pr - fromIntegral (floor pr :: Int)
      prc = pc - fromIntegral (floor pc :: Int)

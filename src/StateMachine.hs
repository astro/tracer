{-# LANGUAGE MultiParamTypeClasses #-}

module StateMachine where

import Affection

import Types

import Load
import Menu.Connect
import Menu.Adjust
import MainGame.WorldMap
import MainGame.MindMap
import Util

instance StateMachine UserData State where
  smLoad Load = loadLoad
  smLoad (Menu Connect) = loadMenu
  smLoad (Menu (Adjust a c)) = (\ud -> loadAdjust ud a c (loadMenu ud))
  smLoad (Main _) = loadMap

  smUpdate Load = updateLoad
  smUpdate (Menu Connect) = updateMenu
  smUpdate (Menu (Adjust _ _)) = const $ const (return ())
  smUpdate (Main WorldMap) = updateMap
  smUpdate (Main MindMap) = updateMind

  smDraw Load = drawLoad
  smDraw (Menu Connect) = drawMenu
  smDraw (Menu (Adjust _ _)) = (\ud -> drawMenu ud >> drawAdjust (nano ud))
  smDraw (Main WorldMap) = drawMap
  smDraw (Main MindMap) = drawMind

  smEvent _ ud evs = do
    let Subsystems w m k j _ = subsystems ud
    _ <- consumeSDLEvents j =<<
      consumeSDLEvents k =<<
      consumeSDLEvents w =<<
      consumeSDLEvents m evs
    return ()

  smClean _ = fullClean

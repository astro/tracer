{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Object.ActionTime where

import Affection as A

import Data.String

-- internal imports

import Types

instance ActionTime ObjType ObjState where
  actionTime ObjCopier "copying" = 5
  actionTime ObjComputer "off" = 0
  actionTime ObjComputer "on" = 20
  actionTime ObjComputer "hack" = 20
  actionTime ObjDoor     "open" = 2

  actionTime o s = A.log
    Error
    (fromString (show o) <> ": " <> fromString s <> ": has not a time")
    0

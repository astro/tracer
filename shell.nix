{ pkgs ? import <nixpkgs> {}}:

let
  # create a modified haskell package set with my package inside it and broken dependencies
  # jailbroken.
  hpkgs = pkgs.haskellPackages.override {
    overrides = hself: hsuper: {
      tracer-game = hself.callCabal2nix "tracer-game" (gitignore ./.) {};
      affection = hself.callCabal2nix "affection" (gitignore ../affection) {};
    };
  };
  gitignore = dir: pkgs.nix-gitignore.gitignoreSource [] dir;
in
  hpkgs.tracer-game.env
